#!/usr/bin/env python
from crhelper import CfnResource
from tempfile import mkstemp
from os.path import join
import boto3
import subprocess
import logging
import os
import random
import string
import yaml

LAMBDA_TASK_ROOT = os.environ['LAMBDA_TASK_ROOT']
AWS_DEFAULT_REGION = os.environ['AWS_REGION']
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_SESSION_TOKEN = os.environ['AWS_SESSION_TOKEN']
logger = logging.getLogger(__name__)
# Initialise the helper, all inputs are optional, this example shows the defaults
helper = CfnResource(json_logging=False, log_level='DEBUG', boto_level='CRITICAL')

try:
    session = boto3.session.Session()
    client = boto3.client('eks')
except Exception as e:
    helper.init_failure(e)


def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def get_properties(event):
    return event['ResourceProperties']


def generate_kubeconfig(event):
    properties = get_properties(event)

    cluster = client.describe_cluster(name=properties['Cluster'])
    cluster_cert = cluster["cluster"]["certificateAuthority"]["data"]
    cluster_ep = cluster["cluster"]["endpoint"]

    return {
        "apiVersion": "v1",
        "kind": "Config",
        "clusters": [
            {
                "cluster": {
                    "server": "%s" % cluster_ep,
                    "certificate-authority-data": "%s" % cluster_cert
                },
                "name": "kubernetes"
            }
        ],
        "contexts": [
            {
                "context": {
                    "cluster": "kubernetes",
                    "user": "aws"
                },
                "name": "aws"
            }
        ],
        "current-context": "aws",
        "preferences": {},
        "users": [
            {
                "name": "aws",
                "user": {
                    "exec": {
                        "apiVersion": "client.authentication.k8s.io/v1alpha1",
                        "command": "%s" % join(LAMBDA_TASK_ROOT, 'bin/aws'),
                        "args": [
                            "eks",
                            "get-token",
                            "--cluster-name",
                            properties['Cluster']
                        ]
                    }
                }
            }
        ]
    }


def kubeconfig_to_file(config):
    config_text = yaml.dump(config, default_flow_style=False)
    fd, path = mkstemp()
    with open(fd, 'w') as f:
        f.write(config_text)
    return path


def _upsert(event, action='upsert'):
    properties = get_properties(event)
    map_flag = '--mapusers'
    arn_flag = '--userarn'
    arn = properties.get('UserArn')
    if properties.get('RoleArn'):
        map_flag = '--maproles'
        arn_flag = '--rolearn'
        arn = properties.get('RoleArn')

    command = [
            join(LAMBDA_TASK_ROOT, 'bin/aws-auth'),
            action,
            map_flag,
            arn_flag, arn,
            '--username', properties['UserName'],
            '--groups', ','.join(properties['Groups']),
            '--retry',
            '--retry-max-count', '10',
        ]
    p = subprocess.Popen(
        command,
        env={
            "KUBECONFIG": kubeconfig_to_file(generate_kubeconfig(event)),
            "PYTHONPATH": LAMBDA_TASK_ROOT,
            "AWS_DEFAULT_REGION": AWS_DEFAULT_REGION,
            "AWS_ACCESS_KEY_ID": AWS_ACCESS_KEY_ID,
            "AWS_SECRET_ACCESS_KEY": AWS_SECRET_ACCESS_KEY,
            "AWS_SESSION_TOKEN": AWS_SESSION_TOKEN,
        },
    )
    p.wait()
    if p.returncode != 0:
        raise ValueError('Command %s failed with exit code %s' % (' '.join(command), p.returncode))


@helper.create
def create(event, context):
    logger.info("Got Create")
    # Optionally return an ID that will be used for the resource PhysicalResourceId,
    # if None is returned an ID will be generated. If a poll_create function is defined
    # return value is placed into the poll event as event['CrHelperData']['PhysicalResourceId']
    #
    # To add response data update the helper.Data dict
    # If poll is enabled data is placed into poll event as event['CrHelperData']
    # Grab data from environment
    resource_id = "%s" % get_random_string(12)
    resource_id = resource_id.upper().replace('-', '').replace('_', '')
    _upsert(event, action='upsert')
    return resource_id


@helper.update
def update(event, context):
    logger.info("Got Update")
    # If the update resulted in a new resource being created, return an id for the new resource. CloudFormation will send
    # a delete event with the old id when stack update completes
    _upsert(event, action='upsert')


@helper.delete
def delete(event, context):
    logger.info("Got Delete")
    _upsert(event, action='remove')


def handler(event, context):
    helper(event, context)
