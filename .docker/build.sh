#!/usr/bin/env bash

set -e

# /var/task

yum install -y zip

pip3 install -r /src/requirements.txt -t .
cp /src/*.py .

mkdir -p bin
cp /build/aws-auth/aws-auth ./bin/

#curl -o ./bin/aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.17.7/2020-07-08/bin/linux/amd64/aws-iam-authenticator
#chmod +x ./bin/aws-iam-authenticator

GLOBIGNORE=.:..; zip -9 -r /out/lambda * ; unset GLOBIGNORE
