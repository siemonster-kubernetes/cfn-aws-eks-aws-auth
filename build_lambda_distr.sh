#!/usr/bin/env bash
set -e

docker build -t cfn-aws-eks-aws-auth-golang -f Dockerfile.aws-auth .
docker run --rm -it -v "$(pwd)/build/aws-auth:/src" cfn-aws-eks-aws-auth-golang /bin/bash -c 'cp /go/bin/aws-auth /src'
docker run --rm -it -v "$(pwd)/build:/build:ro" -v "$(pwd):/src:ro" -v "$(pwd)/lambda:/out" --user=root --entrypoint /src/.docker/build.sh lambci/lambda:python3.7
mv lambda/lambda.zip lambda/cfn-aws-eks-aws-auth.py_v1.zip
